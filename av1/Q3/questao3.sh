#!/bin/bash

echo "Variáveis no Bash podem ser criadas de várias maneiras."

echo "Atribuição direta:"
a=10
echo "Variável a: $a"

echo "Atribuição com comando:"
b=$(date)
echo "Variável b (data atual): $b"

echo "Atribuição com comando de substituição de processos:"
c=`pwd`
echo "Variável c (diretório atual): $c"

echo "Recebendo um valor do usuário:"
echo "Digite o valor da variável d:"
read d
echo "Variável d: $d"

echo "Recebendo valor como parâmetro de linha de comando:"
echo "Variável e: $1"

echo "Variáveis automáticas são variáveis especiais que são automaticamente definidas pelo shell."
echo "Algumas das variáveis automáticas mais comuns são:"
echo "\$0: O nome do script"
echo "\$1, \$2, ...: Parâmetros de linha de comando"
echo "\$#: Número de parâmetros de linha de comando"
echo "\$*: Todos os parâmetros de linha de comando como uma única string"
echo "\$@: Todos os parâmetros de linha de comando como uma lista separada: $@"
