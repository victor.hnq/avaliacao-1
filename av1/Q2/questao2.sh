#!/bin/bash


a="/tmp"
b="/etc"
c="/bin"

A=$(ls -l "$a" | grep -c '^d')
B=$(ls -l "$b" | grep -c '^d')
C=$(ls -l "$c" | grep -c '^d')

echo "Quantidade de arquivos e diretórios em $a: $A"
echo "Quantidade de arquivos e diretórios em $b: $B"
echo "Quantidade de arquivos e diretórios em $c: $C"
