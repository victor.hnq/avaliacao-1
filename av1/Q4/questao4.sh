#!/bin/bash

echo "Informações sobre o hardware do computador:"

echo "CPU:"
grep "model name" /proc/cpuinfo | cut -d':' -f2 | head -n 1
grep "cpu cores" /proc/cpuinfo | cut -d':' -f2 | head -n 1
grep "cpu MHz" /proc/cpuinfo | cut -d':' -f2 | head -n 1

echo "Memória RAM:"
free -h | awk '/Mem:/{print "Total:", $2, "| Em uso:", $3, "| Livre:", $4, "| Porcentagem em uso:", ($3/$2)*100"%"}'

echo "Disco:"
df -h --total | awk '/total/{print "Total:", $2, "| Utilizado:", $3, "| Livre:", $4, "| Porcentagem utilizado:", $5}'

echo "Placa de Rede:"
ip addr show | awk '/^[0-9]/{print "Interface:", $2}'

echo "Placa de Vídeo:"
lspci | awk '/VGA/{print "Modelo:", $3, $4, $5, $6}'

