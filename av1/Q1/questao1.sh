#!/bin/bash

echo -e " \033[1;33m Nem tudo que é ouro fulgura, \033[0m"
sleep 2

echo -e " \033[1;34m Nem todo o vagante é vadio; \033[0m"
sleep 2

echo -e " \033[1;35m O velho que é forte não murcha,\033[0m"
sleep 2

echo -e " \033[0;33m Raízes profundas não são atingidas pela geada.  \033[0m"
sleep 2

echo -e " \033[0;31m Das cinzas um fogo há de vir.  \033[0m"
