#!/bin/bash

echo "Digite 4 nomes para criar diretórios:"
read -p "Nome 1: " a
read -p "Nome 2: " b
read -p "Nome 3: " c
read -p "Nome 4: " d

mkdir "$a" "$b" "$c" "$d"

d=$(date +"%d-%m-%Y")

echo -e "# $a\nData: $d" > "$a/README.md"
echo -e "# $a\nData: $d" > "$b/README.md"
echo -e "# $c\nData: $d" > "$c/README.md"
echo -e "# $d\nData: $d" > "$d/README.md"
