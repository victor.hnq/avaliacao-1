#!/bin/bash

d=$(date)

p=$(date -d "next Wednesday" "+%d-%m-%Y")

s=$(date -d "next Wednesday +7 days" "+%d-%m-%Y")

echo "Próxima quarta-feira: $p"
echo "Quarta-feira seguinte: $s"
